-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "07/12/2021 21:17:51"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-11\ IS
    PORT (
	P1 : OUT std_logic;
	A1 : IN std_logic;
	B1 : IN std_logic;
	P2 : OUT std_logic;
	A2 : IN std_logic;
	B2 : IN std_logic;
	P3 : OUT std_logic;
	A3 : IN std_logic;
	B3 : IN std_logic;
	P4 : OUT std_logic;
	A4 : IN std_logic;
	B4 : IN std_logic;
	P5 : OUT std_logic;
	P6 : OUT std_logic;
	P7 : OUT std_logic;
	P8 : OUT std_logic
	);
END \aula-11\;

-- Design Ports Information
-- P1	=>  Location: PIN_T22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P2	=>  Location: PIN_N21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P3	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P4	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P5	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P6	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P7	=>  Location: PIN_M22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P8	=>  Location: PIN_P22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B1	=>  Location: PIN_T15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B2	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B3	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A3	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B4	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A4	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-11\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_P1 : std_logic;
SIGNAL ww_A1 : std_logic;
SIGNAL ww_B1 : std_logic;
SIGNAL ww_P2 : std_logic;
SIGNAL ww_A2 : std_logic;
SIGNAL ww_B2 : std_logic;
SIGNAL ww_P3 : std_logic;
SIGNAL ww_A3 : std_logic;
SIGNAL ww_B3 : std_logic;
SIGNAL ww_P4 : std_logic;
SIGNAL ww_A4 : std_logic;
SIGNAL ww_B4 : std_logic;
SIGNAL ww_P5 : std_logic;
SIGNAL ww_P6 : std_logic;
SIGNAL ww_P7 : std_logic;
SIGNAL ww_P8 : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \A1~input_o\ : std_logic;
SIGNAL \B1~input_o\ : std_logic;
SIGNAL \inst|inst~combout\ : std_logic;
SIGNAL \A2~input_o\ : std_logic;
SIGNAL \B2~input_o\ : std_logic;
SIGNAL \inst4|inst2|inst14~0_combout\ : std_logic;
SIGNAL \A3~input_o\ : std_logic;
SIGNAL \inst5|inst2|inst14~0_combout\ : std_logic;
SIGNAL \B3~input_o\ : std_logic;
SIGNAL \inst8|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst8|inst2|inst13~0_combout\ : std_logic;
SIGNAL \A4~input_o\ : std_logic;
SIGNAL \inst6|inst2|inst14~0_combout\ : std_logic;
SIGNAL \B4~input_o\ : std_logic;
SIGNAL \inst9|inst~combout\ : std_logic;
SIGNAL \inst12|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst9|inst2|inst16~0_combout\ : std_logic;
SIGNAL \inst7|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst10|inst~combout\ : std_logic;
SIGNAL \inst10|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst9|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst13|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst14|inst~combout\ : std_logic;
SIGNAL \inst10|inst2|inst18~0_combout\ : std_logic;
SIGNAL \inst13|inst2|inst16~0_combout\ : std_logic;
SIGNAL \inst6|inst2|inst18~0_combout\ : std_logic;
SIGNAL \inst11|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst13|inst2|inst18~0_combout\ : std_logic;
SIGNAL \inst14|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst11|inst2|inst18~0_combout\ : std_logic;
SIGNAL \inst13|inst2|inst18~1_combout\ : std_logic;
SIGNAL \inst11|inst2|inst14~1_combout\ : std_logic;
SIGNAL \inst15|inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst15|inst2|inst18~0_combout\ : std_logic;
SIGNAL \ALT_INV_A4~input_o\ : std_logic;
SIGNAL \ALT_INV_B4~input_o\ : std_logic;
SIGNAL \ALT_INV_A3~input_o\ : std_logic;
SIGNAL \ALT_INV_B3~input_o\ : std_logic;
SIGNAL \ALT_INV_A2~input_o\ : std_logic;
SIGNAL \ALT_INV_B2~input_o\ : std_logic;
SIGNAL \ALT_INV_B1~input_o\ : std_logic;
SIGNAL \ALT_INV_A1~input_o\ : std_logic;
SIGNAL \inst11|inst2|ALT_INV_inst18~0_combout\ : std_logic;
SIGNAL \inst11|inst2|ALT_INV_inst14~1_combout\ : std_logic;
SIGNAL \inst13|inst2|ALT_INV_inst18~1_combout\ : std_logic;
SIGNAL \inst11|inst2|ALT_INV_inst14~0_combout\ : std_logic;
SIGNAL \inst6|inst2|ALT_INV_inst18~0_combout\ : std_logic;
SIGNAL \inst13|inst2|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst13|inst2|ALT_INV_inst18~0_combout\ : std_logic;
SIGNAL \inst10|inst2|ALT_INV_inst18~0_combout\ : std_logic;
SIGNAL \inst14|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst10|inst2|ALT_INV_inst14~0_combout\ : std_logic;
SIGNAL \inst9|inst2|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst7|inst2|ALT_INV_inst14~0_combout\ : std_logic;
SIGNAL \inst10|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst9|inst2|ALT_INV_inst14~0_combout\ : std_logic;
SIGNAL \inst6|inst2|ALT_INV_inst14~0_combout\ : std_logic;
SIGNAL \inst8|inst2|ALT_INV_inst13~0_combout\ : std_logic;
SIGNAL \inst9|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst5|inst2|ALT_INV_inst14~0_combout\ : std_logic;

BEGIN

P1 <= ww_P1;
ww_A1 <= A1;
ww_B1 <= B1;
P2 <= ww_P2;
ww_A2 <= A2;
ww_B2 <= B2;
P3 <= ww_P3;
ww_A3 <= A3;
ww_B3 <= B3;
P4 <= ww_P4;
ww_A4 <= A4;
ww_B4 <= B4;
P5 <= ww_P5;
P6 <= ww_P6;
P7 <= ww_P7;
P8 <= ww_P8;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_A4~input_o\ <= NOT \A4~input_o\;
\ALT_INV_B4~input_o\ <= NOT \B4~input_o\;
\ALT_INV_A3~input_o\ <= NOT \A3~input_o\;
\ALT_INV_B3~input_o\ <= NOT \B3~input_o\;
\ALT_INV_A2~input_o\ <= NOT \A2~input_o\;
\ALT_INV_B2~input_o\ <= NOT \B2~input_o\;
\ALT_INV_B1~input_o\ <= NOT \B1~input_o\;
\ALT_INV_A1~input_o\ <= NOT \A1~input_o\;
\inst11|inst2|ALT_INV_inst18~0_combout\ <= NOT \inst11|inst2|inst18~0_combout\;
\inst11|inst2|ALT_INV_inst14~1_combout\ <= NOT \inst11|inst2|inst14~1_combout\;
\inst13|inst2|ALT_INV_inst18~1_combout\ <= NOT \inst13|inst2|inst18~1_combout\;
\inst11|inst2|ALT_INV_inst14~0_combout\ <= NOT \inst11|inst2|inst14~0_combout\;
\inst6|inst2|ALT_INV_inst18~0_combout\ <= NOT \inst6|inst2|inst18~0_combout\;
\inst13|inst2|ALT_INV_inst16~0_combout\ <= NOT \inst13|inst2|inst16~0_combout\;
\inst13|inst2|ALT_INV_inst18~0_combout\ <= NOT \inst13|inst2|inst18~0_combout\;
\inst10|inst2|ALT_INV_inst18~0_combout\ <= NOT \inst10|inst2|inst18~0_combout\;
\inst14|ALT_INV_inst~combout\ <= NOT \inst14|inst~combout\;
\inst10|inst2|ALT_INV_inst14~0_combout\ <= NOT \inst10|inst2|inst14~0_combout\;
\inst9|inst2|ALT_INV_inst16~0_combout\ <= NOT \inst9|inst2|inst16~0_combout\;
\inst7|inst2|ALT_INV_inst14~0_combout\ <= NOT \inst7|inst2|inst14~0_combout\;
\inst10|ALT_INV_inst~combout\ <= NOT \inst10|inst~combout\;
\inst9|inst2|ALT_INV_inst14~0_combout\ <= NOT \inst9|inst2|inst14~0_combout\;
\inst6|inst2|ALT_INV_inst14~0_combout\ <= NOT \inst6|inst2|inst14~0_combout\;
\inst8|inst2|ALT_INV_inst13~0_combout\ <= NOT \inst8|inst2|inst13~0_combout\;
\inst9|ALT_INV_inst~combout\ <= NOT \inst9|inst~combout\;
\inst5|inst2|ALT_INV_inst14~0_combout\ <= NOT \inst5|inst2|inst14~0_combout\;

-- Location: IOOBUF_X54_Y15_N39
\P1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst~combout\,
	devoe => ww_devoe,
	o => ww_P1);

-- Location: IOOBUF_X54_Y18_N96
\P2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_P2);

-- Location: IOOBUF_X54_Y16_N22
\P3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_P3);

-- Location: IOOBUF_X54_Y19_N5
\P4~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_P4);

-- Location: IOOBUF_X54_Y19_N56
\P5~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_P5);

-- Location: IOOBUF_X54_Y16_N39
\P6~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_P6);

-- Location: IOOBUF_X54_Y19_N39
\P7~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst15|inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_P7);

-- Location: IOOBUF_X54_Y16_N56
\P8~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst15|inst2|inst18~0_combout\,
	devoe => ww_devoe,
	o => ww_P8);

-- Location: IOIBUF_X54_Y17_N21
\A1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1,
	o => \A1~input_o\);

-- Location: IOIBUF_X54_Y15_N4
\B1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B1,
	o => \B1~input_o\);

-- Location: LABCELL_X52_Y17_N0
\inst|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst~combout\ = ( \B1~input_o\ & ( \A1~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A1~input_o\,
	datae => \ALT_INV_B1~input_o\,
	combout => \inst|inst~combout\);

-- Location: IOIBUF_X54_Y17_N4
\A2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2,
	o => \A2~input_o\);

-- Location: IOIBUF_X54_Y18_N78
\B2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B2,
	o => \B2~input_o\);

-- Location: LABCELL_X52_Y17_N39
\inst4|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst2|inst14~0_combout\ = ( \B1~input_o\ & ( \B2~input_o\ & ( !\A2~input_o\ $ (!\A1~input_o\) ) ) ) # ( !\B1~input_o\ & ( \B2~input_o\ & ( \A1~input_o\ ) ) ) # ( \B1~input_o\ & ( !\B2~input_o\ & ( \A2~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100001111000011110101101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datac => \ALT_INV_A1~input_o\,
	datae => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst4|inst2|inst14~0_combout\);

-- Location: IOIBUF_X54_Y16_N4
\A3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A3,
	o => \A3~input_o\);

-- Location: LABCELL_X52_Y17_N42
\inst5|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst5|inst2|inst14~0_combout\ = ( \B2~input_o\ & ( (!\B1~input_o\ & (((\A2~input_o\)))) # (\B1~input_o\ & (!\A3~input_o\ $ (((!\A2~input_o\) # (\A1~input_o\))))) ) ) # ( !\B2~input_o\ & ( (\A3~input_o\ & \B1~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100001111010110010000111101011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A3~input_o\,
	datab => \ALT_INV_A1~input_o\,
	datac => \ALT_INV_A2~input_o\,
	datad => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst5|inst2|inst14~0_combout\);

-- Location: IOIBUF_X54_Y17_N38
\B3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B3,
	o => \B3~input_o\);

-- Location: LABCELL_X53_Y17_N30
\inst8|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|inst2|inst14~0_combout\ = ( \B3~input_o\ & ( !\A1~input_o\ $ (!\inst5|inst2|inst14~0_combout\) ) ) # ( !\B3~input_o\ & ( \inst5|inst2|inst14~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100110011110011000011001111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A1~input_o\,
	datad => \inst5|inst2|ALT_INV_inst14~0_combout\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst8|inst2|inst14~0_combout\);

-- Location: LABCELL_X52_Y17_N48
\inst8|inst2|inst13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|inst2|inst13~0_combout\ = ( \B1~input_o\ & ( \B2~input_o\ & ( (\A1~input_o\ & (\A3~input_o\ & \B3~input_o\)) ) ) ) # ( !\B1~input_o\ & ( \B2~input_o\ & ( (\A2~input_o\ & (\A1~input_o\ & \B3~input_o\)) ) ) ) # ( \B1~input_o\ & ( !\B2~input_o\ & ( 
-- (\A1~input_o\ & (\A3~input_o\ & \B3~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000001100000000000100010000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \ALT_INV_A1~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_B3~input_o\,
	datae => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst8|inst2|inst13~0_combout\);

-- Location: IOIBUF_X54_Y17_N55
\A4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A4,
	o => \A4~input_o\);

-- Location: LABCELL_X52_Y17_N24
\inst6|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst2|inst14~0_combout\ = ( \B1~input_o\ & ( \B2~input_o\ & ( !\A4~input_o\ $ (((!\A2~input_o\ & ((!\A3~input_o\))) # (\A2~input_o\ & ((!\A1~input_o\) # (\A3~input_o\))))) ) ) ) # ( !\B1~input_o\ & ( \B2~input_o\ & ( \A3~input_o\ ) ) ) # ( 
-- \B1~input_o\ & ( !\B2~input_o\ & ( \A4~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100001111000011110001101011100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \ALT_INV_A1~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_A4~input_o\,
	datae => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst6|inst2|inst14~0_combout\);

-- Location: IOIBUF_X54_Y15_N21
\B4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B4,
	o => \B4~input_o\);

-- Location: LABCELL_X53_Y17_N6
\inst9|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst9|inst~combout\ = (\B3~input_o\ & \A2~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B3~input_o\,
	datac => \ALT_INV_A2~input_o\,
	combout => \inst9|inst~combout\);

-- Location: LABCELL_X53_Y17_N33
\inst12|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst12|inst2|inst14~0_combout\ = ( \inst9|inst~combout\ & ( !\inst8|inst2|inst13~0_combout\ $ (!\inst6|inst2|inst14~0_combout\ $ (((!\A1~input_o\) # (!\B4~input_o\)))) ) ) # ( !\inst9|inst~combout\ & ( !\inst8|inst2|inst13~0_combout\ $ 
-- (!\inst6|inst2|inst14~0_combout\ $ (((\A1~input_o\ & \B4~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001101001010110100110100110100101100101101010010110010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|inst2|ALT_INV_inst13~0_combout\,
	datab => \ALT_INV_A1~input_o\,
	datac => \inst6|inst2|ALT_INV_inst14~0_combout\,
	datad => \ALT_INV_B4~input_o\,
	dataf => \inst9|ALT_INV_inst~combout\,
	combout => \inst12|inst2|inst14~0_combout\);

-- Location: LABCELL_X52_Y17_N6
\inst9|inst2|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst9|inst2|inst16~0_combout\ = ( \B1~input_o\ & ( \B2~input_o\ & ( (\A2~input_o\ & (\A1~input_o\ & (\A3~input_o\ & \B3~input_o\))) ) ) ) # ( !\B1~input_o\ & ( \B2~input_o\ & ( (\A2~input_o\ & (\A1~input_o\ & \B3~input_o\)) ) ) ) # ( \B1~input_o\ & ( 
-- !\B2~input_o\ & ( (\A2~input_o\ & (\A1~input_o\ & (\A3~input_o\ & \B3~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000100000000000100010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \ALT_INV_A1~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_B3~input_o\,
	datae => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst9|inst2|inst16~0_combout\);

-- Location: LABCELL_X52_Y17_N30
\inst7|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst2|inst14~0_combout\ = ( \B1~input_o\ & ( \B2~input_o\ & ( (!\A3~input_o\ & (\A4~input_o\ & ((!\A2~input_o\) # (!\A1~input_o\)))) # (\A3~input_o\ & (\A2~input_o\ & ((!\A4~input_o\)))) ) ) ) # ( !\B1~input_o\ & ( \B2~input_o\ & ( \A4~input_o\ ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000010111100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \ALT_INV_A1~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_A4~input_o\,
	datae => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst7|inst2|inst14~0_combout\);

-- Location: LABCELL_X52_Y17_N45
\inst10|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|inst~combout\ = ( \B3~input_o\ & ( \A3~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A3~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst10|inst~combout\);

-- Location: LABCELL_X52_Y17_N12
\inst10|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|inst2|inst14~0_combout\ = ( \inst10|inst~combout\ & ( \inst8|inst2|inst13~0_combout\ & ( !\inst7|inst2|inst14~0_combout\ $ (((\inst9|inst2|inst16~0_combout\) # (\inst6|inst2|inst14~0_combout\))) ) ) ) # ( !\inst10|inst~combout\ & ( 
-- \inst8|inst2|inst13~0_combout\ & ( !\inst7|inst2|inst14~0_combout\ $ (((!\inst6|inst2|inst14~0_combout\ & !\inst9|inst2|inst16~0_combout\))) ) ) ) # ( \inst10|inst~combout\ & ( !\inst8|inst2|inst13~0_combout\ & ( !\inst7|inst2|inst14~0_combout\ $ 
-- ((((\inst6|inst2|inst14~0_combout\ & \inst9|inst~combout\)) # (\inst9|inst2|inst16~0_combout\))) ) ) ) # ( !\inst10|inst~combout\ & ( !\inst8|inst2|inst13~0_combout\ & ( !\inst7|inst2|inst14~0_combout\ $ (((!\inst9|inst2|inst16~0_combout\ & 
-- ((!\inst6|inst2|inst14~0_combout\) # (!\inst9|inst~combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011111001000110010000011011101110111100010001000100001110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst2|ALT_INV_inst14~0_combout\,
	datab => \inst9|inst2|ALT_INV_inst16~0_combout\,
	datac => \inst9|ALT_INV_inst~combout\,
	datad => \inst7|inst2|ALT_INV_inst14~0_combout\,
	datae => \inst10|ALT_INV_inst~combout\,
	dataf => \inst8|inst2|ALT_INV_inst13~0_combout\,
	combout => \inst10|inst2|inst14~0_combout\);

-- Location: LABCELL_X53_Y17_N12
\inst9|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst9|inst2|inst14~0_combout\ = ( \inst9|inst~combout\ & ( !\inst6|inst2|inst14~0_combout\ $ (\inst8|inst2|inst13~0_combout\) ) ) # ( !\inst9|inst~combout\ & ( !\inst6|inst2|inst14~0_combout\ $ (!\inst8|inst2|inst13~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110000000011111111000011110000000011111111000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|inst2|ALT_INV_inst14~0_combout\,
	datad => \inst8|inst2|ALT_INV_inst13~0_combout\,
	dataf => \inst9|ALT_INV_inst~combout\,
	combout => \inst9|inst2|inst14~0_combout\);

-- Location: LABCELL_X53_Y17_N15
\inst13|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst13|inst2|inst14~0_combout\ = ( \A2~input_o\ & ( !\inst10|inst2|inst14~0_combout\ $ (((!\B4~input_o\) # ((\inst9|inst2|inst14~0_combout\ & \A1~input_o\)))) ) ) # ( !\A2~input_o\ & ( !\inst10|inst2|inst14~0_combout\ $ 
-- (((!\inst9|inst2|inst14~0_combout\) # ((!\A1~input_o\) # (!\B4~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010110010101010101011001010101101010010101010110101001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst10|inst2|ALT_INV_inst14~0_combout\,
	datab => \inst9|inst2|ALT_INV_inst14~0_combout\,
	datac => \ALT_INV_A1~input_o\,
	datad => \ALT_INV_B4~input_o\,
	dataf => \ALT_INV_A2~input_o\,
	combout => \inst13|inst2|inst14~0_combout\);

-- Location: LABCELL_X53_Y17_N48
\inst14|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst14|inst~combout\ = (\A3~input_o\ & \B4~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A3~input_o\,
	datac => \ALT_INV_B4~input_o\,
	combout => \inst14|inst~combout\);

-- Location: LABCELL_X52_Y17_N18
\inst10|inst2|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|inst2|inst18~0_combout\ = ( \inst10|inst~combout\ & ( \inst8|inst2|inst13~0_combout\ & ( (!\inst6|inst2|inst14~0_combout\ & (!\inst9|inst2|inst16~0_combout\ & !\inst7|inst2|inst14~0_combout\)) ) ) ) # ( !\inst10|inst~combout\ & ( 
-- \inst8|inst2|inst13~0_combout\ & ( (!\inst7|inst2|inst14~0_combout\) # ((!\inst6|inst2|inst14~0_combout\ & !\inst9|inst2|inst16~0_combout\)) ) ) ) # ( \inst10|inst~combout\ & ( !\inst8|inst2|inst13~0_combout\ & ( (!\inst9|inst2|inst16~0_combout\ & 
-- (!\inst7|inst2|inst14~0_combout\ & ((!\inst6|inst2|inst14~0_combout\) # (!\inst9|inst~combout\)))) ) ) ) # ( !\inst10|inst~combout\ & ( !\inst8|inst2|inst13~0_combout\ & ( (!\inst7|inst2|inst14~0_combout\) # ((!\inst9|inst2|inst16~0_combout\ & 
-- ((!\inst6|inst2|inst14~0_combout\) # (!\inst9|inst~combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111001000110010000000000011111111100010001000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst2|ALT_INV_inst14~0_combout\,
	datab => \inst9|inst2|ALT_INV_inst16~0_combout\,
	datac => \inst9|ALT_INV_inst~combout\,
	datad => \inst7|inst2|ALT_INV_inst14~0_combout\,
	datae => \inst10|ALT_INV_inst~combout\,
	dataf => \inst8|inst2|ALT_INV_inst13~0_combout\,
	combout => \inst10|inst2|inst18~0_combout\);

-- Location: LABCELL_X53_Y17_N0
\inst13|inst2|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst13|inst2|inst16~0_combout\ = ( \inst5|inst2|inst14~0_combout\ & ( \B3~input_o\ & ( (\A2~input_o\ & (\inst6|inst2|inst14~0_combout\ & (\B4~input_o\ & \A1~input_o\))) ) ) ) # ( !\inst5|inst2|inst14~0_combout\ & ( \B3~input_o\ & ( (\A2~input_o\ & 
-- (!\inst6|inst2|inst14~0_combout\ & (\B4~input_o\ & \A1~input_o\))) ) ) ) # ( \inst5|inst2|inst14~0_combout\ & ( !\B3~input_o\ & ( (\A2~input_o\ & (\inst6|inst2|inst14~0_combout\ & (\B4~input_o\ & \A1~input_o\))) ) ) ) # ( !\inst5|inst2|inst14~0_combout\ & 
-- ( !\B3~input_o\ & ( (\A2~input_o\ & (\inst6|inst2|inst14~0_combout\ & (\B4~input_o\ & \A1~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000000000000000100000000000001000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \inst6|inst2|ALT_INV_inst14~0_combout\,
	datac => \ALT_INV_B4~input_o\,
	datad => \ALT_INV_A1~input_o\,
	datae => \inst5|inst2|ALT_INV_inst14~0_combout\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst13|inst2|inst16~0_combout\);

-- Location: LABCELL_X52_Y17_N54
\inst6|inst2|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst2|inst18~0_combout\ = ( \B1~input_o\ & ( \B2~input_o\ & ( (!\A2~input_o\ & (((\A3~input_o\ & \A4~input_o\)))) # (\A2~input_o\ & (((\A1~input_o\ & \A4~input_o\)) # (\A3~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000010100011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \ALT_INV_A1~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_A4~input_o\,
	datae => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst6|inst2|inst18~0_combout\);

-- Location: LABCELL_X53_Y17_N9
\inst11|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst11|inst2|inst14~0_combout\ = ( \B2~input_o\ & ( (\A4~input_o\ & (!\inst6|inst2|inst18~0_combout\ $ (!\B3~input_o\))) ) ) # ( !\B2~input_o\ & ( (\B3~input_o\ & \A4~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000011001100000000001100110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst2|ALT_INV_inst18~0_combout\,
	datab => \ALT_INV_B3~input_o\,
	datad => \ALT_INV_A4~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst11|inst2|inst14~0_combout\);

-- Location: LABCELL_X53_Y17_N24
\inst13|inst2|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst13|inst2|inst18~0_combout\ = ( \inst5|inst2|inst14~0_combout\ & ( \B3~input_o\ & ( (\B4~input_o\ & (((!\inst6|inst2|inst14~0_combout\ & \A1~input_o\)) # (\A2~input_o\))) ) ) ) # ( !\inst5|inst2|inst14~0_combout\ & ( \B3~input_o\ & ( (\B4~input_o\ & 
-- (((\inst6|inst2|inst14~0_combout\ & \A1~input_o\)) # (\A2~input_o\))) ) ) ) # ( \inst5|inst2|inst14~0_combout\ & ( !\B3~input_o\ & ( (\B4~input_o\ & (((\inst6|inst2|inst14~0_combout\ & \A1~input_o\)) # (\A2~input_o\))) ) ) ) # ( 
-- !\inst5|inst2|inst14~0_combout\ & ( !\B3~input_o\ & ( (\B4~input_o\ & (((\inst6|inst2|inst14~0_combout\ & \A1~input_o\)) # (\A2~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000111000001010000011100000101000001110000010100001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datab => \inst6|inst2|ALT_INV_inst14~0_combout\,
	datac => \ALT_INV_B4~input_o\,
	datad => \ALT_INV_A1~input_o\,
	datae => \inst5|inst2|ALT_INV_inst14~0_combout\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst13|inst2|inst18~0_combout\);

-- Location: LABCELL_X53_Y17_N36
\inst14|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst14|inst2|inst14~0_combout\ = ( \inst10|inst2|inst14~0_combout\ & ( \inst13|inst2|inst18~0_combout\ & ( !\inst14|inst~combout\ $ (!\inst10|inst2|inst18~0_combout\ $ (\inst11|inst2|inst14~0_combout\)) ) ) ) # ( !\inst10|inst2|inst14~0_combout\ & ( 
-- \inst13|inst2|inst18~0_combout\ & ( !\inst14|inst~combout\ $ (!\inst10|inst2|inst18~0_combout\ $ (!\inst13|inst2|inst16~0_combout\ $ (\inst11|inst2|inst14~0_combout\))) ) ) ) # ( \inst10|inst2|inst14~0_combout\ & ( !\inst13|inst2|inst18~0_combout\ & ( 
-- !\inst14|inst~combout\ $ (!\inst10|inst2|inst18~0_combout\ $ (!\inst13|inst2|inst16~0_combout\ $ (\inst11|inst2|inst14~0_combout\))) ) ) ) # ( !\inst10|inst2|inst14~0_combout\ & ( !\inst13|inst2|inst18~0_combout\ & ( !\inst14|inst~combout\ $ 
-- (!\inst10|inst2|inst18~0_combout\ $ (!\inst13|inst2|inst16~0_combout\ $ (\inst11|inst2|inst14~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001011001101001100101100110100110010110011010010110011010011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst14|ALT_INV_inst~combout\,
	datab => \inst10|inst2|ALT_INV_inst18~0_combout\,
	datac => \inst13|inst2|ALT_INV_inst16~0_combout\,
	datad => \inst11|inst2|ALT_INV_inst14~0_combout\,
	datae => \inst10|inst2|ALT_INV_inst14~0_combout\,
	dataf => \inst13|inst2|ALT_INV_inst18~0_combout\,
	combout => \inst14|inst2|inst14~0_combout\);

-- Location: LABCELL_X53_Y17_N45
\inst11|inst2|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst11|inst2|inst18~0_combout\ = ( \B2~input_o\ & ( (\A4~input_o\ & ((!\inst6|inst2|inst18~0_combout\ & (!\inst10|inst2|inst18~0_combout\ & \B3~input_o\)) # (\inst6|inst2|inst18~0_combout\ & ((!\inst10|inst2|inst18~0_combout\) # (\B3~input_o\))))) ) ) # 
-- ( !\B2~input_o\ & ( (!\inst10|inst2|inst18~0_combout\ & (\B3~input_o\ & \A4~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110000000000010011010000000001001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst2|ALT_INV_inst18~0_combout\,
	datab => \inst10|inst2|ALT_INV_inst18~0_combout\,
	datac => \ALT_INV_B3~input_o\,
	datad => \ALT_INV_A4~input_o\,
	dataf => \ALT_INV_B2~input_o\,
	combout => \inst11|inst2|inst18~0_combout\);

-- Location: LABCELL_X53_Y17_N51
\inst13|inst2|inst18~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst13|inst2|inst18~1_combout\ = ( \inst13|inst2|inst18~0_combout\ & ( (!\inst10|inst2|inst14~0_combout\ & !\inst13|inst2|inst16~0_combout\) ) ) # ( !\inst13|inst2|inst18~0_combout\ & ( !\inst13|inst2|inst16~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000010101010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst10|inst2|ALT_INV_inst14~0_combout\,
	datad => \inst13|inst2|ALT_INV_inst16~0_combout\,
	dataf => \inst13|inst2|ALT_INV_inst18~0_combout\,
	combout => \inst13|inst2|inst18~1_combout\);

-- Location: LABCELL_X53_Y17_N42
\inst11|inst2|inst14~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst11|inst2|inst14~1_combout\ = ( \inst11|inst2|inst14~0_combout\ & ( \inst10|inst2|inst18~0_combout\ ) ) # ( !\inst11|inst2|inst14~0_combout\ & ( !\inst10|inst2|inst18~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst10|inst2|ALT_INV_inst18~0_combout\,
	dataf => \inst11|inst2|ALT_INV_inst14~0_combout\,
	combout => \inst11|inst2|inst14~1_combout\);

-- Location: LABCELL_X53_Y17_N18
\inst15|inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst14~0_combout\ = ( \A4~input_o\ & ( \inst11|inst2|inst14~1_combout\ & ( !\inst11|inst2|inst18~0_combout\ $ (((!\B4~input_o\ & (\inst13|inst2|inst18~1_combout\)) # (\B4~input_o\ & ((!\inst13|inst2|inst18~1_combout\) # (\A3~input_o\))))) ) 
-- ) ) # ( !\A4~input_o\ & ( \inst11|inst2|inst14~1_combout\ & ( !\inst11|inst2|inst18~0_combout\ $ (((\inst13|inst2|inst18~1_combout\ & ((!\B4~input_o\) # (!\A3~input_o\))))) ) ) ) # ( \A4~input_o\ & ( !\inst11|inst2|inst14~1_combout\ & ( 
-- !\inst11|inst2|inst18~0_combout\ $ (((!\B4~input_o\) # ((!\inst13|inst2|inst18~1_combout\ & \A3~input_o\)))) ) ) ) # ( !\A4~input_o\ & ( !\inst11|inst2|inst14~1_combout\ & ( !\inst11|inst2|inst18~0_combout\ $ (((!\B4~input_o\) # ((!\A3~input_o\) # 
-- (\inst13|inst2|inst18~1_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001101100011011001100011011011000011110001101001011010010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B4~input_o\,
	datab => \inst11|inst2|ALT_INV_inst18~0_combout\,
	datac => \inst13|inst2|ALT_INV_inst18~1_combout\,
	datad => \ALT_INV_A3~input_o\,
	datae => \ALT_INV_A4~input_o\,
	dataf => \inst11|inst2|ALT_INV_inst14~1_combout\,
	combout => \inst15|inst2|inst14~0_combout\);

-- Location: LABCELL_X53_Y17_N54
\inst15|inst2|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst18~0_combout\ = ( \A4~input_o\ & ( \inst11|inst2|inst14~1_combout\ & ( (!\B4~input_o\ & (\inst11|inst2|inst18~0_combout\ & (!\inst13|inst2|inst18~1_combout\))) # (\B4~input_o\ & (((!\inst13|inst2|inst18~1_combout\) # (\A3~input_o\)) # 
-- (\inst11|inst2|inst18~0_combout\))) ) ) ) # ( !\A4~input_o\ & ( \inst11|inst2|inst14~1_combout\ & ( (\inst11|inst2|inst18~0_combout\ & ((!\inst13|inst2|inst18~1_combout\) # ((\B4~input_o\ & \A3~input_o\)))) ) ) ) # ( \A4~input_o\ & ( 
-- !\inst11|inst2|inst14~1_combout\ & ( (\B4~input_o\ & (((!\inst13|inst2|inst18~1_combout\ & \A3~input_o\)) # (\inst11|inst2|inst18~0_combout\))) ) ) ) # ( !\A4~input_o\ & ( !\inst11|inst2|inst14~1_combout\ & ( (\B4~input_o\ & 
-- (\inst11|inst2|inst18~0_combout\ & (!\inst13|inst2|inst18~1_combout\ & \A3~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000100010101000100110000001100010111000101110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B4~input_o\,
	datab => \inst11|inst2|ALT_INV_inst18~0_combout\,
	datac => \inst13|inst2|ALT_INV_inst18~1_combout\,
	datad => \ALT_INV_A3~input_o\,
	datae => \ALT_INV_A4~input_o\,
	dataf => \inst11|inst2|ALT_INV_inst14~1_combout\,
	combout => \inst15|inst2|inst18~0_combout\);

-- Location: LABCELL_X26_Y32_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


