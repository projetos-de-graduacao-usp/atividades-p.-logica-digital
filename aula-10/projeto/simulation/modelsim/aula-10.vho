-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "07/04/2021 16:22:57"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-10\ IS
    PORT (
	S1 : OUT std_logic;
	operacao : IN std_logic;
	A1 : IN std_logic;
	B1 : IN std_logic;
	S2 : OUT std_logic;
	A2 : IN std_logic;
	B2 : IN std_logic;
	S3 : OUT std_logic;
	A3 : IN std_logic;
	B3 : IN std_logic;
	S4 : OUT std_logic;
	A4 : IN std_logic;
	B4 : IN std_logic;
	Cout : OUT std_logic
	);
END \aula-10\;

-- Design Ports Information
-- S1	=>  Location: PIN_AB20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S3	=>  Location: PIN_Y14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S4	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Cout	=>  Location: PIN_AA12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1	=>  Location: PIN_U13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B1	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- operacao	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B2	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A3	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B3	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A4	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B4	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-10\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_S1 : std_logic;
SIGNAL ww_operacao : std_logic;
SIGNAL ww_A1 : std_logic;
SIGNAL ww_B1 : std_logic;
SIGNAL ww_S2 : std_logic;
SIGNAL ww_A2 : std_logic;
SIGNAL ww_B2 : std_logic;
SIGNAL ww_S3 : std_logic;
SIGNAL ww_A3 : std_logic;
SIGNAL ww_B3 : std_logic;
SIGNAL ww_S4 : std_logic;
SIGNAL ww_A4 : std_logic;
SIGNAL ww_B4 : std_logic;
SIGNAL ww_Cout : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \B1~input_o\ : std_logic;
SIGNAL \A1~input_o\ : std_logic;
SIGNAL \inst|inst14~0_combout\ : std_logic;
SIGNAL \A2~input_o\ : std_logic;
SIGNAL \B2~input_o\ : std_logic;
SIGNAL \operacao~input_o\ : std_logic;
SIGNAL \inst1|inst14~0_combout\ : std_logic;
SIGNAL \B3~input_o\ : std_logic;
SIGNAL \inst1|inst18~0_combout\ : std_logic;
SIGNAL \A3~input_o\ : std_logic;
SIGNAL \inst3|inst14~0_combout\ : std_logic;
SIGNAL \A4~input_o\ : std_logic;
SIGNAL \B4~input_o\ : std_logic;
SIGNAL \inst4|inst14~0_combout\ : std_logic;
SIGNAL \inst4|inst18~0_combout\ : std_logic;
SIGNAL \ALT_INV_B4~input_o\ : std_logic;
SIGNAL \ALT_INV_A4~input_o\ : std_logic;
SIGNAL \ALT_INV_B3~input_o\ : std_logic;
SIGNAL \ALT_INV_A3~input_o\ : std_logic;
SIGNAL \ALT_INV_B2~input_o\ : std_logic;
SIGNAL \ALT_INV_operacao~input_o\ : std_logic;
SIGNAL \ALT_INV_A2~input_o\ : std_logic;
SIGNAL \ALT_INV_B1~input_o\ : std_logic;
SIGNAL \ALT_INV_A1~input_o\ : std_logic;
SIGNAL \inst1|ALT_INV_inst18~0_combout\ : std_logic;

BEGIN

S1 <= ww_S1;
ww_operacao <= operacao;
ww_A1 <= A1;
ww_B1 <= B1;
S2 <= ww_S2;
ww_A2 <= A2;
ww_B2 <= B2;
S3 <= ww_S3;
ww_A3 <= A3;
ww_B3 <= B3;
S4 <= ww_S4;
ww_A4 <= A4;
ww_B4 <= B4;
Cout <= ww_Cout;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_B4~input_o\ <= NOT \B4~input_o\;
\ALT_INV_A4~input_o\ <= NOT \A4~input_o\;
\ALT_INV_B3~input_o\ <= NOT \B3~input_o\;
\ALT_INV_A3~input_o\ <= NOT \A3~input_o\;
\ALT_INV_B2~input_o\ <= NOT \B2~input_o\;
\ALT_INV_operacao~input_o\ <= NOT \operacao~input_o\;
\ALT_INV_A2~input_o\ <= NOT \A2~input_o\;
\ALT_INV_B1~input_o\ <= NOT \B1~input_o\;
\ALT_INV_A1~input_o\ <= NOT \A1~input_o\;
\inst1|ALT_INV_inst18~0_combout\ <= NOT \inst1|inst18~0_combout\;

-- Location: IOOBUF_X40_Y0_N93
\S1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S1);

-- Location: IOOBUF_X36_Y0_N2
\S2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst1|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S2);

-- Location: IOOBUF_X36_Y0_N19
\S3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S3);

-- Location: IOOBUF_X33_Y0_N76
\S4~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S4);

-- Location: IOOBUF_X29_Y0_N36
\Cout~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|inst18~0_combout\,
	devoe => ww_devoe,
	o => ww_Cout);

-- Location: IOIBUF_X36_Y0_N35
\B1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B1,
	o => \B1~input_o\);

-- Location: IOIBUF_X33_Y0_N41
\A1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1,
	o => \A1~input_o\);

-- Location: MLABCELL_X34_Y1_N0
\inst|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst14~0_combout\ = ( \A1~input_o\ & ( !\B1~input_o\ ) ) # ( !\A1~input_o\ & ( \B1~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111111100001111000000001111000011111111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_B1~input_o\,
	datae => \ALT_INV_A1~input_o\,
	combout => \inst|inst14~0_combout\);

-- Location: IOIBUF_X33_Y0_N58
\A2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2,
	o => \A2~input_o\);

-- Location: IOIBUF_X36_Y0_N52
\B2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B2,
	o => \B2~input_o\);

-- Location: IOIBUF_X38_Y0_N52
\operacao~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_operacao,
	o => \operacao~input_o\);

-- Location: MLABCELL_X34_Y1_N39
\inst1|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst1|inst14~0_combout\ = ( \A1~input_o\ & ( \operacao~input_o\ & ( !\A2~input_o\ $ (!\B2~input_o\) ) ) ) # ( !\A1~input_o\ & ( \operacao~input_o\ & ( !\B1~input_o\ $ (!\A2~input_o\ $ (\B2~input_o\)) ) ) ) # ( \A1~input_o\ & ( !\operacao~input_o\ & ( 
-- !\B1~input_o\ $ (!\A2~input_o\ $ (\B2~input_o\)) ) ) ) # ( !\A1~input_o\ & ( !\operacao~input_o\ & ( !\A2~input_o\ $ (!\B2~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110000010110101010010101011010101001010000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_A2~input_o\,
	datad => \ALT_INV_B2~input_o\,
	datae => \ALT_INV_A1~input_o\,
	dataf => \ALT_INV_operacao~input_o\,
	combout => \inst1|inst14~0_combout\);

-- Location: IOIBUF_X34_Y0_N52
\B3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B3,
	o => \B3~input_o\);

-- Location: MLABCELL_X34_Y1_N42
\inst1|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst1|inst18~0_combout\ = ( \A1~input_o\ & ( \operacao~input_o\ & ( (!\B2~input_o\) # (\A2~input_o\) ) ) ) # ( !\A1~input_o\ & ( \operacao~input_o\ & ( (!\B2~input_o\ & ((!\B1~input_o\) # (\A2~input_o\))) # (\B2~input_o\ & (\A2~input_o\ & !\B1~input_o\)) 
-- ) ) ) # ( \A1~input_o\ & ( !\operacao~input_o\ & ( (!\B2~input_o\ & (\A2~input_o\ & \B1~input_o\)) # (\B2~input_o\ & ((\B1~input_o\) # (\A2~input_o\))) ) ) ) # ( !\A1~input_o\ & ( !\operacao~input_o\ & ( (\B2~input_o\ & \A2~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001000101110001011110110010101100101011101110111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_A2~input_o\,
	datac => \ALT_INV_B1~input_o\,
	datae => \ALT_INV_A1~input_o\,
	dataf => \ALT_INV_operacao~input_o\,
	combout => \inst1|inst18~0_combout\);

-- Location: IOIBUF_X34_Y0_N1
\A3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A3,
	o => \A3~input_o\);

-- Location: MLABCELL_X34_Y1_N21
\inst3|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst3|inst14~0_combout\ = ( \operacao~input_o\ & ( !\B3~input_o\ $ (!\inst1|inst18~0_combout\ $ (!\A3~input_o\)) ) ) # ( !\operacao~input_o\ & ( !\B3~input_o\ $ (!\inst1|inst18~0_combout\ $ (\A3~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110100101101001011010010110100110010110100101101001011010010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B3~input_o\,
	datab => \inst1|ALT_INV_inst18~0_combout\,
	datac => \ALT_INV_A3~input_o\,
	dataf => \ALT_INV_operacao~input_o\,
	combout => \inst3|inst14~0_combout\);

-- Location: IOIBUF_X34_Y0_N18
\A4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A4,
	o => \A4~input_o\);

-- Location: IOIBUF_X34_Y0_N35
\B4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B4,
	o => \B4~input_o\);

-- Location: MLABCELL_X34_Y1_N54
\inst4|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst14~0_combout\ = ( \B4~input_o\ & ( \operacao~input_o\ & ( !\A4~input_o\ $ (((!\B3~input_o\ & (!\A3~input_o\ & !\inst1|inst18~0_combout\)) # (\B3~input_o\ & ((!\A3~input_o\) # (!\inst1|inst18~0_combout\))))) ) ) ) # ( !\B4~input_o\ & ( 
-- \operacao~input_o\ & ( !\A4~input_o\ $ (((!\B3~input_o\ & ((\inst1|inst18~0_combout\) # (\A3~input_o\))) # (\B3~input_o\ & (\A3~input_o\ & \inst1|inst18~0_combout\)))) ) ) ) # ( \B4~input_o\ & ( !\operacao~input_o\ & ( !\A4~input_o\ $ (((!\B3~input_o\ & 
-- (\A3~input_o\ & \inst1|inst18~0_combout\)) # (\B3~input_o\ & ((\inst1|inst18~0_combout\) # (\A3~input_o\))))) ) ) ) # ( !\B4~input_o\ & ( !\operacao~input_o\ & ( !\A4~input_o\ $ (((!\B3~input_o\ & ((!\A3~input_o\) # (!\inst1|inst18~0_combout\))) # 
-- (\B3~input_o\ & (!\A3~input_o\ & !\inst1|inst18~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001111001111000111000011000011111010010010010110010110110110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B3~input_o\,
	datab => \ALT_INV_A3~input_o\,
	datac => \ALT_INV_A4~input_o\,
	datad => \inst1|ALT_INV_inst18~0_combout\,
	datae => \ALT_INV_B4~input_o\,
	dataf => \ALT_INV_operacao~input_o\,
	combout => \inst4|inst14~0_combout\);

-- Location: MLABCELL_X34_Y1_N30
\inst4|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst18~0_combout\ = ( \B4~input_o\ & ( \operacao~input_o\ & ( (\A4~input_o\ & ((!\B3~input_o\ & ((\inst1|inst18~0_combout\) # (\A3~input_o\))) # (\B3~input_o\ & (\A3~input_o\ & \inst1|inst18~0_combout\)))) ) ) ) # ( !\B4~input_o\ & ( 
-- \operacao~input_o\ & ( ((!\B3~input_o\ & ((\inst1|inst18~0_combout\) # (\A3~input_o\))) # (\B3~input_o\ & (\A3~input_o\ & \inst1|inst18~0_combout\))) # (\A4~input_o\) ) ) ) # ( \B4~input_o\ & ( !\operacao~input_o\ & ( ((!\B3~input_o\ & (\A3~input_o\ & 
-- \inst1|inst18~0_combout\)) # (\B3~input_o\ & ((\inst1|inst18~0_combout\) # (\A3~input_o\)))) # (\A4~input_o\) ) ) ) # ( !\B4~input_o\ & ( !\operacao~input_o\ & ( (\A4~input_o\ & ((!\B3~input_o\ & (\A3~input_o\ & \inst1|inst18~0_combout\)) # (\B3~input_o\ 
-- & ((\inst1|inst18~0_combout\) # (\A3~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000111000111110111111100101111101111110000001000001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B3~input_o\,
	datab => \ALT_INV_A3~input_o\,
	datac => \ALT_INV_A4~input_o\,
	datad => \inst1|ALT_INV_inst18~0_combout\,
	datae => \ALT_INV_B4~input_o\,
	dataf => \ALT_INV_operacao~input_o\,
	combout => \inst4|inst18~0_combout\);

-- Location: MLABCELL_X49_Y7_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


